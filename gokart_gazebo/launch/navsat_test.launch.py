import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

"""
    * How does ROS2 Launch know where my gazebo world is?
        Within the ign_gazebo.launch.py you can specifiy the absolute path to the directory
        containing the models.
"""
def generate_launch_description():

    gokart_launch = get_package_share_directory('gokart_gazebo')

    ign_gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(gokart_launch, 'launch', 'ign_gazebo.launch.py')),
        launch_arguments={
            'ign_args': 'navsat_test.world'
        }.items(),
    )

    """
    rviz = Node(
       package='rviz2',
       executable='rviz2',
       arguments=['-d', os.path.join(class_launch, 'rviz', 'harv_simulation.rviz')],
       condition=IfCondition(LaunchConfiguration('rviz')),
    )
    """

    """
        The meaning of the symbols after the ROS message type
        @ is a bidirectional bridge.
        [ is a bridge from Ignition to ROS.
        ] is a bridge from ROS to Ignition.
        Format:
            topic_name@ros message name([ or ] or @)ignition message name
    """
    bridge = Node(
        package='ros_ign_bridge',
        executable='parameter_bridge',
        arguments=['/model/navigator_agk/cmd_vel@geometry_msgs/msg/Twist]ignition.msgs.Twist',
                   '/navsat@sensor_msgs/msg/NavSatFix[ignition.msgs.NavSat',
                   '/clock@rosgraph_msgs/msg/Clock[ignition.msgs.Clock'],
        output='screen'
    )

    return LaunchDescription([
        ign_gazebo,
        DeclareLaunchArgument('rviz', default_value='true',
                              description='Open RViz.'),
        bridge
    ])