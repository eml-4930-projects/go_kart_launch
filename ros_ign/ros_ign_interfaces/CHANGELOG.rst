^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package ros_ign_interfaces
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.221.2 (2021-07-20)
--------------------
* [ros2]  new package ros_ign_interfaces, provide some  Ignition-specific ROS messages. (`#152 <https://github.com/osrf/ros_ign/issues/152>`_)
  * add new package ros_ign_interfaces,provide some Ignition-specific ros .msg and .srv files
* Contributors: gezp

