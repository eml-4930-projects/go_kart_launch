# Setup
Clone this repository in the __src__ folder of your workspace.

```bash
git clone https://gitlab.com/eml-4930-projects/class_resources.git
```
### Updates
This repository may occasional be updated. To pull the updates run the command shown below while in the directory __class_resources__
```bash
git pull origin master
```
Rebuild the workspace after performing this git pull.

## Requirements
* Ubuntu 20.04
* ROS2 Foxy
* Ignition Gazebo Fortress

## Install ROS2 Foxy
You should have this already installed but [incase you don't](https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html)

## Install Ignition Gazebo Fortress

Follow [the instructions](https://ignitionrobotics.org/docs/fortress/install_ubuntu) to install ignition gazebo fortress.

## Build Repository
Navigate to your root workspace directory and run the following commands.
```bash
rosdep install -r --from-paths src -i -y --rosdistro foxy
```
This might ask your password in order to download required dependencies

```bash
colcon build
```